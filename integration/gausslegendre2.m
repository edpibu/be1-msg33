%%% Gauss-Legendre à 2 points
% Calcule l'intégrale de la fonction f entre a et b par la méthode de
% Gauss-Legendre à 2 points
% Arguments: f (fonction), a (nombre), b (nombre)
function I = gausslegendre2(f,a,b)
cv = @(x) (b-a)/2*x + (a+b)/2;
I = (b-a)/2 * (1*f(cv(-1/sqrt(3))) + 1*f(cv(1/sqrt(3))));
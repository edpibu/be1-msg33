%%% Point-Milieu
% Calcule l'intégrale de la fonction f entre a et b par la méthode du
% point milieu
% Arguments: f (fonction), a (nombre), b (nombre)
function I = pointmilieu(f,a,b)
I = (b-a) * f((a+b)/2);
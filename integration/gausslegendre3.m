%%% Gauss-Legendre à 3 points
% Calcule l'intégrale de la fonction f entre a et b par la méthode de
% Gauss-Legendre à 3 points
% Arguments: f (fonction), a (nombre), b (nombre)
function I = gausslegendre3(f,a,b)
cv = @(x) (b-a)/2*x + (a+b)/2;
I = (b-a)/2 * (5/9*f(cv(-sqrt(3/5)))...
    + 8/9*f(cv(0))...
    + 5/9*f(cv(sqrt(3/5))));
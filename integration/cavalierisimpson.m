%%% Cavalieri-Simpson
% Calcule l'intégrale de la fonction f entre a et b par la méthode de
% Cavalieri-Simpson
% Arguments: f (fonction), a (nombre), b (nombre)
function I = cavalierisimpson(f,a,b)
I = 1/6 * (b-a) * (f(a) + 4*f((a+b)/2) + f(b));
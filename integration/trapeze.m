%%% Trapèze
% Calcule l'intégrale de la fonction f entre a et b par la méthode du
% trapèze
% Arguments: f (fonction), a (nombre), b (nombre)
function I = trapeze(f,a,b)
I = (b-a) * (f(a) + f(b)) / 2;
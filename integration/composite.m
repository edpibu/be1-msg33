%%% Composite
% Cette fonction permet de déterminer l'intégrale de f entre a et b en
% utilisant une méthode composite sur n intervalles de la méthode methode
% donnée en argument.
% Elle renvoie aussi en second argument l'erreur réalisée par rapport à une
% valeur de référence Iref fournie en argument.
%
% Arguments: f (fonction), a (nombre), b (nombre), n (entier), methode
% (fonction), Iref (optionnel, nombre)
function [I, Err] = composite(f, a, b, n, methode, Iref)
x = linspace(a, b, n+1);
I = 0;
for i = 1:n
    I = I + methode(f, x(i), x(i+1));
end

if nargin > 5
    Err = abs((Iref-I) / Iref); 
end
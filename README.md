# BE1 MSG33
Ce projet a pour objectif d'implémenter différentes méthodes de calcul numérique d'intégrales et de les tester sur différentes applications du Génie Civil.

## Auteurs
Edgar P. Burkhart et Thibault Laurent

## Utilisation
Tous les code de ce projet sont des code MatLab, testés sur MatLab R2019b.

### Méthodes d'intégration simples
Les méthodes d'intégration s'utilisent toutes avec la syntaxe suivante:
```(matlab)
I = methode(f, a, b)
```

Avec comme paramètres:
- `f`: la fonction à intégrer,
- `a`, `b`: les bornes d'intégration,

et comme valeur de retour:
- `I`: la valeur de l'intégrale calculée.

Exemple:
```(matlab)
I = trapeze(@(x) x^2, 0, 1) % Retourne 1/2
```

#### Méthodes disponibles
Les méthodes d'intégration implémentées dans ce projet sont les suivantes:
- `pointmilieu`: méthode du point milieu,
- `trapeze`: méthode du trapèze,
- `cavalierisimpson`: méthode de Cavalieri-Simpson,
- `gausslegendre2`: méthode de Gauss-Legendre à 2 points,
- `gausslegendre3`: méthode de Gauss-Legendre à 3 points.

### Méthodes d'intégration composite
Une fonction permettant de réaliser une intégration par méthode composite des méthodes
précédentes est implémentée, et peut être utilisée des manières suivantes:
```(matlab)
I = composite(f, a, b, n, methode)
[I, Err] = composite(f, a, b, n, methode, Iref)
```
Avec comme paramètres:
- `f`, `a` et `b` ont la même signification que pour les méthodes simples,
- `n`: le nombre de subdivisions de l'intervalle à réaliser,
- `methode`: la méthode d'intégration à utiliser,
- `Iref` (optionnel): la valeur de référence de l'intégrale,

et comme valeurs de retour:
- `I`: la valeur de l'intégrale calculée,
- `Err`: l'erreur réalisée par rapport à `Iref`.

L'argument `methode` doit être une fonction de trois paramètres `f`, `a` et `b`, retournant
une valeur numérique, comme par exemple les fonctions d'intégration simples présentées précédemment.

### Applications
#### Déplacement vertical à la clé d’un arc en plein cintre à trois articulations

```
arc % Affiche la valeur de déplacement et un graphique montrant la convergence des méthodes d'intégration
```

Ce code permet de tracer pour les méthodes d'intégration composites implémentées précédemment le déplacement
vertical calculé en fonction du nombre d'intervalles d'intégrations. Il affiche aussi la valeur la plus
précise (a priori) obtenue.

Les données du problème sont entrées dans les variables suivantes:
- `F`: effort appliqué,
- `R`: rayon de l'arc,
- `E`: module d'Young,
- `IGz`: fonction donnant l'inertie en fonction de l'angle (déterminée ici par régression polynomiale),
- `A`: fonction donnant la section en fonction de l'angle (déterminée ici par régression polynomiale).

#### Coefficient d’influence d’une charge répartie en surface de massif

```
surface_massif % Trace l'abaque
```

Ce code permet de tracer l'abaque permettant de déterminer la contrainte verticale dans un massif
apportée par une charge répartie circulaire en surface.

Les paramètres du tracé sont entrés dans les variables suivantes:
- `nz`: discrétisation suivant l'axe vertical z,
- `nrr`, `nt`: nombre d'intervalles d'intégration suivant r' et θ respectivement,
- `r`: tableau des rayons de tracé de l'abaque.

%%% Déplacement vertical à la clé d’un arc
%%% en plein cintre à trois articulations
% L'objectif de ce code est de tracer le déplacement vertical calculé en
% fonction du nombre de pas d'intégration pour différentes méthodes.
clf

% Paramètres du problème
F = 100e3;  % Effort appliqué
R = 4;      % Rayon de l'arc
E = 35e9;   % Module d'Young

% Approximation polynomiale des paramètres géométriques à partir de la
% discrétisation connue
polyfitIGz = polyfit(pi/180 * [0 18 36 54 72 90],...
    [2.25e-4 1.83e-4 1.3e-4 8.87e-5 6.67e-5 5.72e-5], 5);
polyfitA = polyfit(pi/180 * [0 18 36 54 72 90],...
    [3e-2 2.8e-2 2.5e-2 2.2e-2 2e-2 1.9e-2], 5);

IGz = @(x) polyval(polyfitIGz, x);
A = @(x) polyval(polyfitA, x);

% Fonction à intégrer
f = @(theta) (cos(theta) + sin(theta)^2) / (A(theta))...
    + R^2 * (1 - cos(theta) - sin(theta))^2 / (IGz(theta));

% Méthodes utilisées
methods = {@pointmilieu
           @trapeze
           @cavalierisimpson
           @gausslegendre2
           @gausslegendre3};
% Nom des méthodes, pour la légende du graphique
methodsNames = ["Point Milieu"
                "Trapèze"
                "Cavalieri-Simpson"
                "Gauss-Legendre 2"
                "Gauss-Legendre 3"];


v = zeros(length(methods), 6);

% Parcours de la liste des méthodes
for i = 1:length(methods)
    for k = 0:5
        composite(f, 0, pi/2, 2^k, methods{i});
        v(i,k+1) = F*R/(2*E) * composite(f, 0, pi/2, 2^k, methods{i});
    end
    
    % Tracé de la flèche en fonction de k
    plot(0:5, v(i,:));
    hold on;
end

legend(methodsNames);
xlabel('k');
ylabel('v (m)');

% Affichage du résultat le plus précis obtenu a priori
fprintf("v = %f m\n", v(5,5))

hold off

%%% Coefficient d’influence d’une charge répartie en surface de massif
% L'objectif de ce code est de tracer l'abaque permettant de déterminer la
% contrainte verticale dans un massif apportée par une charge répartie
% circulaire.
clf;

% Définition des paramètres du problème
nz = 100;   % Nombre de points de calcul sur z

% Nombre de points d'intégration
nrr = 100;
nt = 100;
% Points d'intégration
rr = linspace(0,1,nrr);
theta =linspace(0,2*pi,nt);

%   Rayons tracés sur l'abaque
r = [0 0.5 0.75 1 1.25 1.5 2 2.5 3 4 5 6 7 8 9 10];
nr = length(r);
I = zeros(nr, nz);

% Fonction permettant de calculer la distance entre le point de calcul et
% le point de l'intégrale en fonction de leurs positions respectives
rho = @(r, rr, theta) sqrt(rr^2 + r^2 - 2*rr*r*cos(theta));
% Fonction à intégrer
f = @(r, z, rr, theta) rr * 3/(2*pi)...
    * z^3/(rho(r, rr,theta)^2+z^2)^(5/2);

ax = axes;

% Parcours des rayons
for a = 1:nr
    z = linspace(0,10,nz);
    % Parcours des distances verticales
    for b = 1:nz
        S = 0;    % Valeur de la somme des valeurs de la fonction
        for i = 1:nrr-1 % Somme sur r
            for j = 1:nt-1  % Somme sur theta
                S = S + sum(f(r(a), z(b),...
                    (rr(i)+rr(i))/2, (theta(j)+theta(j+1))/2));
            end
        end

        I(a,b) = 2*pi * S/(nrr * nt) * 100; % Calcul final de l'intégrale
    end
    
    % Tracé en semi-log
    pl = semilogx(I(a,:), z, 'b', 'LineWidth', 1);
    hold on
end

% Ajout de la valeur de r/R sur les courbes
for a = 1:nr
    % Indice du point de tracé
    i = round(a/nr * 3*nz/4);
    % Rotation du texte suivant la courbe
    angle = -atand((z(i+1)-z(i-1)) / (2*(log(I(a,i+1))-log(I(a,i-1)))));
    text(I(a,i),...
        z(i),...
        string(r(a)),...
        'BackgroundColor', 'w',...
        'Color', 'b',...
        'HorizontalAlignment', 'center',...
        'FontSize', 6,...
        'Margin', 1,...
        'Rotation', angle);
end
% Label r/R
text(I(nr/2,nz/2),...
    z(nz/2),...
    'r/R',...
    'BackgroundColor', 'w',...
    'Color', 'b',...
    'HorizontalAlignment', 'center',...
    'FontSize', 8,...
    'Margin', 1);

xlabel("Coefficient d'influence I (%)");
ylabel("z/R");

axis([0.1 100 0 10]);       % Cadrage
set(ax, 'YDir', 'reverse'); % Direction de l'axe vertical
pbaspect([2 1 1]);          % Ratio
grid;                       % Grille

hold off
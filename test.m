%%% Test des méthodes d'intégration
% L'objectif de ce code est de permettre le test des méthodes d'intégration
% implémentées ici
clf;

P = {
    @(x) 2*x-1
    @(x) 2*x^2-x+3
    @(x) 2*x^3-x^2+3*x-7
    @(x) 2*x^4-x^3+3*x^2-7*x+4
    @(x) 2*x^5-x^4+3*x^3-7*x^2+4*x+1
};
nP = length(P);
trueP = [
    14
    190/3
    748/3
    5334/5
    69034/15
];

fs = @(x) x * sin(x);
fr = @(x) 1/(1+x^2);
ft = @(x) sqrt(x);
f = {@fs @fr @ft};
nf = length(f);
bornes = [[0 pi]
          [-1 1]
          [0 1]];
truef = [pi pi/2 2/3];
fINames = ["Is" "Ir" "It"];

methods = {
    @pointmilieu
    @trapeze
    @cavalierisimpson
    @gausslegendre2
    @gausslegendre3
};
n = length(methods);

methodsNames = [
    "Point milieu"
    "Trapèze"
    "Cavalieri-Simpson"
    "Gauss-Legendre à 2 Points"
    "Gauss-Legendre à 3 Points"
];

errP = zeros(n, nP);
a = 3; b = 5;
fprintf("\n=== Erreurs relatives sur un calcul d'intégrales de "...
        + "polynômes ===\n\n");
for i = 1:n
    for j = 1:nP
        errP(i,j) = (methods{i}(P{j}, a, b) - trueP(j)) / trueP(j);
        fprintf("Méthode %s, P%i: %.2e\n", methodsNames(i), j, errP(i,j));
    end
    fprintf("\n");
end

errf = zeros(n, nf);
fprintf("\n=== Erreurs relatives sur des calculs d'intégrales "...
        + "de fonctions ===\n\n");
for i = 1:n
    for j = 1:nf
        errf(i,j) = (methods{i}(f{j}, bornes(j,1), bornes(j,2))...
            - truef(j))...
            / truef(j);
        fprintf("Méthode %s, %s: %.2e\n", methodsNames(i), fINames(j),...
            errf(i,j));
    end
    fprintf("\n");
end

ks = 0:12;
rescomp = zeros(n, nf, length(ks));
errcomp = zeros(n, nf, length(ks));
for j = 1:nf
    subplot(nf,1,j);
    for i = 1:n
        for k = ks
            [rescomp(i,j,k+1), errcomp(i,j,k+1)] = composite(f{j},...
                bornes(j,1), bornes(j,2),...
                2^k, methods{i}, truef(j));
        end
        semilogy(ks, permute(errcomp(i,j,:), [3,2,1]));
        grid;
        hold on;
    end
    legend(methodsNames, 'Location', 'eastoutside');
    xlabel('k');
    ylabel('Erreur relative');
    title('Erreur relative sur le calcul de ' + fINames(j));
end
hold off;


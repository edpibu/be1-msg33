function y = fs(x)
y = x * sin(x);